(** Wrapper around Perl [HTTP::Message] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_HTTP_Message.ml,v 1.5 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

open Pl_HTTP_Headers

let _ = eval "use HTTP::Message"

class http_message sv =

object (self)
  inherit http_headers sv

  method content =
    string_of_sv (call_method sv "content" [])
  method set_content content =
    call_method_void sv "set_content" [sv_of_string content]

end
