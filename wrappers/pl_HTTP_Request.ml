(** Wrapper around Perl [HTTP::Request] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_HTTP_Request.ml,v 1.5 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

open Pl_HTTP_Message
open Pl_URI

let _ = eval "use HTTP::Request"

class http_request sv =

object (self)
  inherit http_message sv

  method sv = sv

  method method_ =
    string_of_sv (call_method sv "method" [])
  method set_method meth =
    call_method_void sv "method" [sv_of_string meth]
  method uri =
    string_of_sv (call_method sv "uri" [])
  method set_uri uri =
    call_method_void sv "uri" [sv_of_string uri]
  method as_string =
    string_of_sv (call_method sv "as_string" [])

end

let new_ meth ?uri_obj ?uri (* ?header ?content *) () =
  let sv =
    match uri_obj, uri with
	None, None ->
	  failwith ("Pl_HTTP_Request.new_ must be called with either a "^
		    "~uri_obj (URI object) or ~uri (string) parameter.")
      | Some (uri_obj : uri), None ->
	  call_class_method "HTTP::Request" "new" [sv_of_string meth;
						   uri_obj#sv]
      | _, Some uri ->
	  call_class_method "HTTP::Request" "new" [sv_of_string meth;
						   sv_of_string uri]
  in
  new http_request sv
