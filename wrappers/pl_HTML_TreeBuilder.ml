(** Wrapper around Perl [HTML::TreeBuilder] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_HTML_TreeBuilder.ml,v 1.4 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

open Pl_HTML_Parser
open Pl_HTML_Element

let _ = eval "use HTML::TreeBuilder"

class html_treebuilder sv =

object (self)
  inherit html_parser sv

  method elementify =
    let sv = call_method sv "elementify" [] in
    new html_element sv
end

(* Note that "new" is a reserved word, so I've appended an _ character. *)
let new_ () =
  let sv = call_class_method "HTML::TreeBuilder" "new" [] in
  new html_treebuilder sv

let new_from_file filename =
  let sv = call_class_method "HTML::TreeBuilder" "new_from_file"
	     [sv_of_string filename] in
  new html_treebuilder sv

let new_from_content content =
  let sv = call_class_method "HTML::TreeBuilder" "new_from_content"
	     [sv_of_string content] in
  new html_treebuilder sv
