(** Wrapper around Perl [HTML::Element] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_HTML_Element.ml,v 1.5 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

let _ = eval "use HTML::Element"

type 'a content_t = Element of 'a | String of string

class html_element sv =

  let rec assocs_of_svlist = function
      [] -> []
    | [x] -> failwith "HTML::Element all_attr returned odd-length list!"
    | svname :: svvalue :: xs ->
	(string_of_sv svname, string_of_sv svvalue) :: assocs_of_svlist xs
  in

  let rec list_of_svlist = function
      [] -> []
    | sv :: xs ->
	string_of_sv sv :: list_of_svlist xs
  in

object (self)

  method sv = sv

  method attr name =
    string_of_sv (call_method sv "attr" [sv_of_string name])
  method set_attr name value =
    call_method_void sv "attr" [sv_of_string name; sv_of_string value]
  method tag =
    string_of_sv (call_method sv "tag" [])
  method set_tag tag =
    call_method_void sv "tag" [sv_of_string tag]
  method parent =
    let sv = call_method sv "parent" [] in
    new html_element sv
  method set_parent (parent : html_element) =
    call_method_void sv "parent" [parent#sv]
  method content_list =
    let svlist = call_method_array sv "content_list" [] in
    List.map
      (fun c ->
	 (* Not very satisfactory, but sv_type fails to discern the type
	  * for some reason. XXX
	  *)
	 let str = string_of_sv c in
	 let marker = "HTML::Element=HASH(" in
	 let marker_len = String.length marker in
	 if String.length str > marker_len &&
	   String.sub str 0 marker_len = marker then
	     Element (new html_element c)
	 else
	   String (string_of_sv c)
      ) svlist
  method all_attr =
    let svlist = call_method_array sv "all_attr" [] in
    assocs_of_svlist svlist
  method all_attr_names =
    let svlist = call_method_array sv "all_attr_names" [] in
    list_of_svlist svlist
  method all_external_attr =
    let svlist = call_method_array sv "all_external_attr" [] in
    assocs_of_svlist svlist
  method all_external_attr_names =
    let svlist = call_method_array sv "all_external_attr_names" [] in
    list_of_svlist svlist

end

(* Note that "new" is a reserved word, so I've appended an _ character. *)
let new_ tag attrs =
  let rec loop = function
      [] -> []
    | (name, value) :: xs -> sv_of_string name :: sv_of_string value :: loop xs
  in
  let sv = call_class_method "HTML::Element" "new"
	     (sv_of_string tag :: loop attrs) in
  new html_element sv
