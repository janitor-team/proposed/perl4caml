(** Wrapper around Perl [Date::Format] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_Date_Format.ml,v 1.2 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

let _ = eval "use Date::Format qw()"

let language lang =
  call_class_method_void "Date::Format" "language" [sv_of_string lang]

(* This is just provided for your convenience so you can pass the resulting
 * list of SVs directly to the second argument of {!Pl_Date_Format.strftime}.
 *)
let localtime () =
  call_array ~fn:"localtime" []

let time2str ?zone templ time =
  let args =
    [sv_of_string templ; sv_of_float time] @
    match zone with
	None -> []
      | Some zone -> [sv_of_string zone] in
  string_of_sv (call ~fn:"Date::Format::time2str" args)

let strftime ?zone templ time =
  let args =
    (sv_of_string templ :: time) @
    match zone with
	None -> []
      | Some zone -> [sv_of_string zone] in
  string_of_sv (call ~fn:"Date::Format::strftime" args)

let ctime ?zone time =
  let args =
    [sv_of_float time] @
    match zone with
	None -> []
      | Some zone -> [sv_of_string zone] in
  string_of_sv (call ~fn:"Date::Format::ctime" args)

let asctime ?zone time =
  let args =
    [sv_of_float time] @
    match zone with
	None -> []
      | Some zone -> [sv_of_string zone] in
  string_of_sv (call ~fn:"Date::Format::asctime" args)
