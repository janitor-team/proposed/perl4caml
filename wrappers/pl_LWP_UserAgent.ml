(** Wrapper around Perl [LWP::UserAgent] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_LWP_UserAgent.ml,v 1.6 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

open Pl_HTTP_Request
open Pl_HTTP_Response
open Pl_HTTP_Cookies

let _ = eval "use LWP::UserAgent"

class lwp_useragent sv =

object (self)

  method simple_request (request : http_request) =
    let sv = call_method sv "simple_request" [request#sv] in
    new http_response sv
  method request (request : http_request) =
    let sv = call_method sv "request" [request#sv] in
    new http_response sv
  method agent =
    string_of_sv (call_method sv "agent" [])
  method set_agent v =
    call_method_void sv "agent" [sv_of_string v]
  method from =
    string_of_sv (call_method sv "from" [])
  method set_from v =
    call_method_void sv "from" [sv_of_string v]
  method cookie_jar =
    let sv = call_method sv "cookie_jar" [] in
    new http_cookies sv
  method set_cookie_jar (v : http_cookies) =
    call_method_void sv "cookie_jar" [v#sv]
  method set_cookie_jar_file filename =
    let hv = hv_empty () in
    hv_set hv "file" (sv_of_string filename);
    call_method_void sv "cookie_jar" [hashref hv]
  method requests_redirectable =
    let sv = call_method sv "requests_redirectable" [] in
    let av = deref_array sv in
    List.map string_of_sv (list_of_av av)
  method set_requests_redirectable methods =
    let av = av_empty () in
    List.iter (av_push av) (List.map sv_of_string methods);
    call_method_void sv "requests_redirectable" [arrayref av]
  method add_requests_redirectable method_ =
    let sv = call_method sv "requests_redirectable" [] in
    let av = deref_array sv in
    av_push av (sv_of_string method_)
  method timeout =
    int_of_sv (call_method sv "timeout" [])
  method set_timeout v =
    call_method_void sv "timeout" [sv_of_int v]
  method parse_head =
    bool_of_sv (call_method sv "parse_head" [])
  method set_parse_head v =
    call_method_void sv "parse_head" [sv_of_bool v]
  method max_size =
    int_of_sv (call_method sv "max_size" [])
  method set_max_size v =
    call_method_void sv "max_size" [sv_of_int v]
  method env_proxy () =
    call_method_void sv "env_proxy" []

end

(* Note that "new" is a reserved word, so I've appended an _ character. *)
let new_ ?agent ?from ?timeout ?use_eval ?parse_head ?max_size
    ?env_proxy ?keep_alive ?cookie_jar ?cookie_jar_file () =
  let args = ref [] in
  let may f = function None -> () | Some v -> f v in
  may (fun v ->
	 args := sv_of_string "agent" :: sv_of_string v :: !args) agent;
  may (fun v ->
	 args := sv_of_string "from" :: sv_of_string v :: !args) from;
  may (fun v ->
	 args := sv_of_string "timeout" :: sv_of_int v :: !args) timeout;
  may (fun v ->
	 args := sv_of_string "use_eval" :: sv_of_bool v :: !args) use_eval;
  may (fun v ->
	 args := sv_of_string "parse_head" :: sv_of_bool v :: !args)parse_head;
  may (fun v ->
	 args := sv_of_string "max_size" :: sv_of_int v :: !args) max_size;
  may (fun v ->
	 args := sv_of_string "env_proxy" :: sv_of_bool v :: !args) env_proxy;
  may (fun v ->
	 args := sv_of_string "keep_alive" :: sv_of_int v :: !args) keep_alive;
  may (fun (v : http_cookies) ->
	 args := sv_of_string "cookie_jar" :: v#sv :: !args) cookie_jar;
  may (fun v ->
	 let hv = hv_empty () in
	 hv_set hv "file" (sv_of_string v);
	 let sv = hashref hv in
	 args := sv_of_string "cookie_jar" :: sv :: !args) cookie_jar_file;
  let sv = call_class_method "LWP::UserAgent" "new" !args in
  new lwp_useragent sv
