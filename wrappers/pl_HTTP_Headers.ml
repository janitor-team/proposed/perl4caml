(** Wrapper around Perl [HTTP::Message] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_HTTP_Headers.ml,v 1.3 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

let _ = eval "use HTTP::Headers"

class http_headers sv =

object (self)

  method sv = sv

  method header key =
    string_of_sv (call_method sv "header" [sv_of_string key])
  method set_header key value =
    call_method_void sv "header" [sv_of_string key; sv_of_string value]

  method as_string =
    string_of_sv (call_method sv "as_string" [])

end
