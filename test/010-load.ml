(* Load Perl interpreter.
 * $Id: 010-load.ml,v 1.1 2005/01/28 23:09:33 rich Exp $
 *)

open Perl

(* The next line does nothing.  It just forces OCaml to actually
 * reference and hence load the Perl module.
 *)
let _ = Perl.int_of_sv;;

(* Check for memory errors. *)
Gc.full_major ()
