(* Reference, dereference.
 * $Id: 140-ref.ml,v 1.2 2005/04/14 13:05:12 rich Exp $
 *)

open Perl

let () =
  let sv = sv_of_int 42 in
  let sv = scalarref sv in
  assert (sv_type sv = SVt_RV);
  assert (reftype sv = SVt_IV);
  let sv = deref sv in
  assert (42 = int_of_sv sv);

  let av = av_of_string_list [ "foo"; "bar" ] in
  let sv = arrayref av in
  assert (sv_type sv = SVt_RV);
  assert (reftype sv = SVt_PVAV);
  let av = deref_array sv in
  assert (2 = av_length av);

  let hv = hv_empty () in
  hv_set hv "foo" (sv_of_int 1);
  hv_set hv "bar" (sv_of_int 2);
  let sv = hashref hv in
  assert (sv_type sv = SVt_RV);
  assert (reftype sv = SVt_PVHV);
  let hv = deref_hash sv in
  assert (1 = int_of_sv (hv_get hv "foo"));
  assert (2 = int_of_sv (hv_get hv "bar"));
;;

Gc.full_major ()
