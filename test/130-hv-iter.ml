(* Thoroughly test HV iteration functions.
 * $Id: 130-hv-iter.ml,v 1.1 2005/01/29 12:22:50 rich Exp $
 *)

open Perl

let () =
  let xs = [ "foo", sv_of_int 1;
	     "bar", sv_of_int 2;
	     "baz", sv_of_int 3;
	     "a", sv_of_int 4 ] in

  let hv = hv_of_assoc xs in
  assert (1 = int_of_sv (hv_get hv "foo"));
  assert (2 = int_of_sv (hv_get hv "bar"));
  assert (3 = int_of_sv (hv_get hv "baz"));
  assert (4 = int_of_sv (hv_get hv "a"));
  assert (not (hv_exists hv "b"));
  assert (not (hv_exists hv "c"));

  let keys = List.sort compare (hv_keys hv) in
  assert (4 = List.length keys);
  assert (["a"; "bar"; "baz"; "foo"] = keys);

  let values = List.sort compare (List.map int_of_sv (hv_values hv)) in
  assert (4 = List.length values);
  assert ([1; 2; 3; 4] = values);

  let xs = assoc_of_hv hv in
  let xs = List.map (fun (k, sv) -> k, int_of_sv sv) xs in
  let xs = List.sort compare xs in
  assert (4 = List.length xs);
  assert ([ "a", 4; "bar", 2; "baz", 3; "foo", 1 ] = xs)
;;
Gc.full_major ()
