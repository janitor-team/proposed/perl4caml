(* Thoroughly test SV-related functions.
 * $Id: 100-sv.ml,v 1.1 2005/01/28 23:09:33 rich Exp $
 *)

open Perl

let () =
  assert (42 = int_of_sv (sv_of_int 42));
  assert (42. = float_of_sv (sv_of_float 42.));
  assert (true = bool_of_sv (sv_of_bool true));
  assert (false = bool_of_sv (sv_of_bool false));
  assert ("42" = string_of_sv (sv_of_string "42"));
  assert ("42" = string_of_sv (sv_of_int 42));
  assert ("1" = string_of_sv (sv_of_bool true));
  (* assert ("" = string_of_sv (sv_of_bool false)); XXX fails XXX *)
  assert (sv_is_true (sv_of_bool true));
  assert (sv_is_true (sv_true ()));
  assert (not (sv_is_true (sv_false ())));
  assert (sv_is_undef (sv_undef ()));

  let sv = sv_undef () in assert (sv_type sv = SVt_NULL);
  let sv = sv_of_int 42 in assert (sv_type sv = SVt_IV);
  (* let sv = sv_of_float 42.1 in assert (sv_type sv = SVt_NV); XXX fails XXX*)
  let sv = sv_of_string "42" in assert (sv_type sv = SVt_PV);
  let sv = eval "\\\"foo\"" in assert (sv_type sv = SVt_RV);

  ignore (eval "$s = 'foo'");
  let sv = get_sv "s" in
  assert ("foo" = string_of_sv sv);
;;

Gc.full_major ()
