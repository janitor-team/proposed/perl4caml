(* Simple eval.
 * $Id: 020-eval.ml,v 1.1 2005/01/28 23:09:33 rich Exp $
 *)

open Perl

let () =
  let sv = eval "2+2" in
  assert (4 = int_of_sv sv);;

Gc.full_major ()
