(* Basic constructor and method calls.
 * $Id: 030-call-method.ml,v 1.1 2005/01/28 23:09:33 rich Exp $
 *)

open Perl

let () =
  ignore (eval "use IO::File");
  let io = call_class_method "IO::File" "new_tmpfile" [] in
  call_method_void io "print" [ sv_of_string "hello, world" ];
  call_method_void io "close" [];;

Gc.full_major ()
