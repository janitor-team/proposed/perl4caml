(* Example program which uses Date::Parse.
 * Copyright (C) 2003 Merjis Ltd.
 * $Id: parsedate.ml,v 1.2 2003/12/11 17:41:52 rich Exp $
 *)

open Printf

open Pl_Date_Parse
open Pl_Date_Format

let () =
  (* Parse dates passed on the command line. *)
  if Array.length Sys.argv <= 1 then
    eprintf "parsedate [list of quoted date strings ...]\n"
  else (
    let strings = List.tl (Array.to_list Sys.argv) in
    List.iter (fun s ->
		 printf "input string = '%s' ->\n" s;
		 let t = str2time s in
		 printf "\ttime_t = %f\n" t;
		 let s = ctime t in
		 printf "\tconverted back to string = %s\n" s;
		 printf "\n"
	      ) strings
  );

  (* Perform a full collection - good way to find GC/allocation bugs. *)
  Gc.full_major ()
