(* Example program which uses Net::Google to query Google.
 * You will need to have a Google API key in ~/.googlekey for this to work.
 * Copyright (C) 2003 Merjis Ltd.
 * $Id: google.ml,v 1.4 2003/12/11 17:41:52 rich Exp $
 *)

open Printf

open Pl_Net_Google

let () =
  (* Load Google API key. *)
  let home = Sys.getenv "HOME" in
  let chan = open_in (home ^ "/.googlekey") in
  let key = input_line chan in
  close_in chan;

  (* Create the Google query object. *)
  let google = Pl_Net_Google.new_ ~key () in

  (* Search. *)
  let search = google#search () in
  search#set_query "merjis";
  search#set_max_results 5;

  printf "Top 5 results for \"merjis\":\n"; flush stdout;

  List.iter
    (fun response ->
       printf "* %s\n  <URL:%s>\n\n" response#title response#url
    ) search#results;

  (* Perform a full collection - good way to find GC/allocation bugs. *)
  Gc.full_major ()
