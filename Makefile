# Interface to Perl from OCaml.
# Copyright (C) 2003 Merjis Ltd.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this library; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#
# $Id: Makefile,v 1.33 2008-03-01 13:02:21 rich Exp $

include Makefile.config

OCAMLC := ocamlc
OCAMLOPT := ocamlopt
OCAMLMKLIB := ocamlmklib
OCAMLDEP := ocamldep
OCAMLDOC := ocamldoc

OCAMLCINCS := -I wrappers
OCAMLOPTINCS := $(OCAMLCINCS)

OCAMLCFLAGS := -w s -g $(OCAMLCINCS)
OCAMLOPTFLAGS := -w s $(OCAMLOPTINCS)

PERLCFLAGS := $(shell perl -e 'use Config; print $$Config{ccflags};')

CC := gcc
CFLAGS := -fPIC -Wall -Wno-unused \
	-I$(OCAMLLIBDIR) \
	-I$(PERLINCDIR) $(PERLCFLAGS) \
	$(EXTRA_CFLAGS) $(EXTRA_EXTRA_CFLAGS)

LIBPERL := $(shell perl -MExtUtils::Embed -e ldopts)

SED := sed

OCAMLDOCFLAGS := -html -stars -sort $(OCAMLCINCS)

WRAPPERS := \
	wrappers/pl_Data_Dumper.cmo \
	wrappers/pl_Date_Calc.cmo \
	wrappers/pl_Date_Format.cmo \
	wrappers/pl_Date_Parse.cmo \
	wrappers/pl_Net_Google_Cache.cmo \
	wrappers/pl_Net_Google_Response.cmo \
	wrappers/pl_Net_Google_Search.cmo \
	wrappers/pl_Net_Google_Spelling.cmo \
	wrappers/pl_Net_Google.cmo \
	wrappers/pl_HTML_Element.cmo \
	wrappers/pl_HTML_Parser.cmo \
	wrappers/pl_HTML_TreeBuilder.cmo \
	wrappers/pl_URI.cmo \
	wrappers/pl_HTTP_Cookies.cmo \
	wrappers/pl_HTTP_Headers.cmo \
	wrappers/pl_HTTP_Message.cmo \
	wrappers/pl_HTTP_Request.cmo \
	wrappers/pl_HTTP_Request_Common.cmo \
	wrappers/pl_HTTP_Response.cmo \
	wrappers/pl_HTML_Form.cmo \
	wrappers/pl_LWP_UserAgent.cmo \
	wrappers/pl_Template.cmo \
	wrappers/pl_WWW_Mechanize.cmo

all:	perl4caml.cma perl4caml.cmxa META all-examples html

perl4caml.cma: perl.cmo perl_c.o $(WRAPPERS)
	$(OCAMLMKLIB) -o perl4caml $(LIBPERL) $^

perl4caml.cmxa: perl.cmx perl_c.o $(WRAPPERS:.cmo=.cmx)
	$(OCAMLMKLIB) -o perl4caml $(LIBPERL) $^

all-examples: examples/test.bc examples/loadpage.bc examples/google.bc \
	examples/test.opt examples/loadpage.opt examples/google.opt \
	examples/parsedate.bc examples/parsedate.opt

TEST_PROGRAMS := $(patsubst %.ml,%.bc,$(wildcard test/*.ml)) \
	$(patsubst %.ml,%.opt,$(wildcard test/*.ml))

test: $(TEST_PROGRAMS) run-tests

check: test

run-tests:
	@fails=0; count=0; \
	export LD_LIBRARY_PATH=`pwd`:$$LD_LIBRARY_PATH; \
	for prog in $(TEST_PROGRAMS); do \
	  if ! $$prog; then \
	    echo Test $$prog failed; \
	    fails=$$(($$fails+1)); \
	  fi; \
	  count=$$(($$count+1)); \
	done; \
	if [ $$fails -eq 0 ]; then \
	  echo All tests succeeded.; \
	  exit 0; \
	else \
	  echo $$fails/$$count tests failed.; \
	  exit 1; \
	fi

%.bc: %.cmo
	$(OCAMLC) $(OCAMLCFLAGS) perl4caml.cma $^ -o $@

%.opt: %.cmx
	$(OCAMLOPT) $(OCAMLOPTFLAGS) -cclib -L. perl4caml.cmxa \
	$(DYNALOADER_HACK) $^ -o $@

%.cmi: %.mli
	$(OCAMLC) $(OCAMLCFLAGS) -c $<

%.cmo: %.ml
	$(OCAMLC) $(OCAMLCFLAGS) -c $<

%.cmx: %.ml
	$(OCAMLOPT) $(OCAMLOPTFLAGS) -c $<

.SUFFIXES: .mli .ml .cmi .cmo .cmx

META:	META.in Makefile.config
	$(SED)  -e 's/@PACKAGE@/$(PACKAGE)/' \
		-e 's/@VERSION@/$(VERSION)/' \
		< $< > $@

# Clean.

JUNKFILES = core *~ *.bak *.cmi *.cmo *.cmx *.cma *.cmxa *.o *.a *.so \
	*.bc *.opt

clean:
	rm -f META
	for d in . examples test wrappers; do \
	  (cd $$d; rm -f $(JUNKFILES)); \
	done

# Build dependencies.

ifeq ($(wildcard .depend),.depend)
include .depend
endif

depend:	.depend

.depend: $(wildcard *.ml) $(wildcard *.mli) $(wildcard examples/*.ml) \
	$(wildcard wrappers/*.ml)
	$(OCAMLDEP) $(OCAMLCINCS) *.mli *.ml examples/*.ml wrappers/*.ml \
	> .depend

# Install.

install:
	rm -rf $(DESTDIR)$(OCAMLLIBDIR)/perl
	install -c -m 0755 -d $(DESTDIR)$(OCAMLLIBDIR)/perl
	install -c -m 0755 -d $(DESTDIR)$(OCAMLLIBDIR)/stublibs
	install -c -m 0644 perl.cmi perl.mli perl4caml.cma perl4caml.cmxa \
	  perl4caml.a libperl4caml.a META \
	  $(WRAPPERS:.cmo=.ml) $(WRAPPERS:.cmo=.cmi) \
	  $(DESTDIR)$(OCAMLLIBDIR)/perl
	install -c -m 0644 dllperl4caml.so $(DESTDIR)$(OCAMLLIBDIR)/stublibs

# Distribution.

dist:
	$(MAKE) check-manifest
	rm -rf $(PACKAGE)-$(VERSION)
	mkdir $(PACKAGE)-$(VERSION)
	tar -cf - -T MANIFEST | tar -C $(PACKAGE)-$(VERSION) -xf -
	tar zcf $(PACKAGE)-$(VERSION).tar.gz $(PACKAGE)-$(VERSION)
	rm -rf $(PACKAGE)-$(VERSION)
	ls -l $(PACKAGE)-$(VERSION).tar.gz

check-manifest:
	@for d in `find -type d -name CVS | grep -v '^\./debian/'`; \
	do \
	b=`dirname $$d`/; \
	awk -F/ '$$1 != "D" {print $$2}' $$d/Entries | \
	sed -e "s|^|$$b|" -e "s|^\./||"; \
	done | sort > .check-manifest; \
	sort MANIFEST > .orig-manifest; \
	diff -u .orig-manifest .check-manifest; rv=$$?; \
	rm -f .orig-manifest .check-manifest; \
	exit $$rv

# Debian packages.

dpkg:
	@if [ 0 != `cvs -q update | wc -l` ]; then \
	echo Please commit all changes to CVS first.; \
	exit 1; \
	fi
	$(MAKE) dist
	rm -rf /tmp/dbuild
	mkdir /tmp/dbuild
	cp $(PACKAGE)-$(VERSION).tar.gz \
	  /tmp/dbuild/$(PACKAGE)_$(VERSION).orig.tar.gz
	export CVSROOT=`cat CVS/Root`; \
	  cd /tmp/dbuild && \
	  cvs export \
	  -d $(PACKAGE)-$(VERSION) \
	  -D now merjis/test/perl4caml
	cd /tmp/dbuild/$(PACKAGE)-$(VERSION) && dpkg-buildpackage -rfakeroot
	rm -rf /tmp/dbuild/$(PACKAGE)-$(VERSION)
	ls -l /tmp/dbuild

# Documentation.

html:	html/index.html

html/index.html: $(wildcard *.ml) $(wildcard *.mli) $(wildcard wrappers/*.ml)
	rm -rf html
	mkdir html
	-$(OCAMLDOC) $(OCAMLDOCFLAGS) -d html $^

.PHONY: depend dist check-manifest html dpkg test run-tests