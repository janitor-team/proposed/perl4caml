perl4caml
Copyright (C) 2003 Merjis Ltd. (http://www.merjis.com/)
$Id: README,v 1.4 2005/01/28 23:09:31 rich Exp $

perl4caml allows you to use Perl code within Objective CAML (OCaml),
thus neatly side-stepping the old problem with OCaml which was that it
lacked a comprehensive set of libraries. Well now you can use any part
of CPAN in your OCaml code.

perl4caml is distributed under the GNU Library General Public License
(see file COPYING.LIB for terms and conditions).

perl4caml was mainly written by Richard W.M. Jones
(rich@annexia.org). See file AUTHORS for other contributors.

	Installation
	------------

(1) You will need the Perl development environment installed. I'm using
    Perl 5.8.x. It is likely that earlier versions may have small
    incompatibilities.

(2) Edit Makefile.config as necessary.

(3) Type 'make'.

(4) It's recommended that you run the automatic tests by using 'make test'.
    You should see 'All tests succeeded.'  If not, please report this
    to me (rich@annexia.org).  If Perl gives any warnings, such as
    'Attempt to free unreferenced scalar', please also report this.

(5) Type 'make install' as root to install.

(6) Try some of the examples in the examples/ directory (some of these
    require that you have certain Perl modules installed).

	Documentation
	-------------

See doc/ for some documentation.

To build ocamldoc (automatically generated documentation for interfaces)
type 'make html'. The output can be found by pointing your browser at
'html/index.html'.
